<div class="container">
    <div class="d-flex justify-content-center align-items-center " style="height: 100vh;">
        <div class="text-center border border-1 border-gray p-5">
            <form method="POST">

                <p>Entrar</p>
                <!-- Email input -->
                <div class="form-outline mb-4">
                    <input type="email" name="email" id="email" class="form-control" />
                    <label class="form-label" for="email">Endereço de email</label>
                </div>

                <!-- Password input -->
                <div class="form-outline mb-4">
                    <input type="password" name="senha" id="senha" class="form-control" />
                    <label class="form-label" for="senha">Senha</label>
                </div>

                <!-- 2 column grid layout for inline styling -->
                <div class="row mb-4">
                    <div class="col d-flex justify-content-center">
                    <!-- Checkbox -->
                    <div class="form-check">
                        <input
                        class="form-check-input"
                        type="checkbox"
                        value=""
                        id="form2Example3"
                        checked
                        />
                        <label class="form-check-label" for="form2Example3"> Me lembrar </label>
                    </div>
                    </div>
                </div>

                <!-- Submit button -->
                <button type="submit" class="btn btn-primary btn-block mb-4">Entrar</button>

                <p class="red-text"><?= $error ? 'Dados de acesso incorretos ' : '' ?></p>
            </form>
        </div>
    </div>
</div>